'use strict';
(function(Game){
	var game = Game.self;
	game.state.add('intro', {
		preload: function(){
			game.load.script('js/scenes/gameplay/gameplay');
			game.load.script('js/scenes/gameplay/sprites/sniper');
			game.load.script('js/scenes/gameplay/sprites/rocket');
			game.load.script('js/scenes/gameplay/sprites/explosion');
			game.load.spritesheet('sniper', 'assets/sniper.png', 53, 63);
			game.load.spritesheet('rocket', 'assets/rocket.png', 26, 49);
			game.load.spritesheet('explosion', 'assets/explosion.png', 128, 128);
		},
		create: function(){
			var clicked = false;
			var button = game.add.sprite(100, 100, 'button');
			button.anchor.setTo(0.5);
			button.x = this.world.centerX;
			button.y = this.world.centerY;
			button.inputEnabled = true;

			button.events.onInputDown.add(function(){
				if (clicked) return;
				button.scale.setTo(1.02);
			});
			button.events.onInputUp.add(function(){
				if (clicked) return;
				clicked = true;
				button.scale.setTo(1);

				var t = this.game.add.tween(button).to({
					alpha: 0
				}, 1000);
				t.onComplete.add(this.startGame, this);
				t.start();
			}, this);
		},
		startGame: function(){
			this.game.state.start('gameplay');
		}
	});
})(window.Game);