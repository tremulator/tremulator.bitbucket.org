'use strict';
(function(Game){
	var game = Game.self;
	var classes = Game.classes;

	var sniper;

	game.state.add('gameplay', {
		preload: function(){
			
		},
		create: function(){
			sniper = new classes.Sniper(this.world.centerX, this.world.centerY + 120).addToScene();
			this.handleClicking();
		},
		handleClicking: function(){
			var leftIsDown = false;
			var rightIsDown = false;

			// prevent context menu on the canvas
			this.game.canvas.oncontextmenu = function(e){
				e.preventDefault();
			};

			this.input.mouse.mouseDownCallback = function(e){
				if (e.button === Phaser.Mouse.LEFT_BUTTON){
					leftIsDown = true;
					return;
				}
				if (e.button === Phaser.Mouse.RIGHT_BUTTON){
					rightIsDown = true;
				}
			};
			this.input.mouse.mouseUpCallback = function(e){
				if (leftIsDown && e.button === Phaser.Mouse.LEFT_BUTTON){
					leftIsDown = false;
					sniper.shootRocket();
					return;
				}
				if (rightIsDown && e.button === Phaser.Mouse.RIGHT_BUTTON){
					rightIsDown = false;
					sniper.walkTo(game.input.x, game.input.y);
				}
			}.bind(this);
		}
	});
})(window.Game);