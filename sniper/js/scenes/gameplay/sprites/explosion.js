(function(Game){
	Game.classes.Explosion = (function(game){
		// constructor
		function Explosion(x, y, onComplete){
			Phaser.Sprite.call(this, game, x, y, 'explosion');
			this.anchor.setTo(0.5);
			this.animations.add('detonating');
			this.visible = false;

			this.animations.getAnimation('detonating').onComplete.add(onComplete);

			game.add.existing(this);
		}

		// extend Phaser.Sprite class
		var p = Explosion.prototype = Object.create(Phaser.Sprite.prototype);

		// public methods
		p.detonate = function(x, y){
			this.x = x;
			this.y = y;
			this.visible = true;
			this.animations.play('detonating', 30);
		};
		p.hide = function(){
			this.visible = false;
		};

		return Explosion;
	})(Game.self);
})(window.Game);