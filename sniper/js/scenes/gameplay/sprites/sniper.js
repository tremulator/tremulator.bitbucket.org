'use strict';
(function(Game){
	// TypeScript inspired class
	Game.classes.Sniper = (function(game){
		// constructor
		function Sniper(x, y){
			Phaser.Sprite.call(this, game, x, y, 'sniper');

			this.defineVars();

			this.anchor.setTo(0.5, 0.6);
			this.animations.add('idle', [0]);
			this.animations.add('walking');

			this.animations.play('idle');
		};

		// extend Phaser.Sprite class
		var p = Sniper.prototype = Object.create(Phaser.Sprite.prototype);
		
		// static properties
		// array where rockets are stored for recycling
		Sniper.rockets = [];

		p.defineVars = function(){
			// distance in pixels to travel in each frame
			this.speed = 3;
			this.movementTween = undefined;
			this.ray = {};
			this.line = game.add.bitmapData(900, 600);
			this.lineSprite = game.add.sprite(0, 0, this.line);
		};

		// public methods
		p.addToScene = function(){
			game.add.existing(this);
			return this;
		};
		p.lookAtPointer = function(){
			// don't rotate the sniper when he's overlapping the cursor
			if (Phaser.Rectangle.contains(this, game.input.x, game.input.y)){
				return;
			}

			// smooth and gradual rotation toward the cursor
			var rotateTo = this.rotateTo = game.physics.arcade.angleToPointer(this) + Math.PI * 0.5;
			var dif = rotateTo - this.rotation;
			if (dif < -Math.PI) dif += Math.PI * 2;
			if (dif > Math.PI) dif -= Math.PI * 2;

			this.rotation += dif / 4;
		};
		p.drawRay = function(){
			var angle = this.rotation - Math.PI * 0.5;

			// offset for the ray so it would visually seem like coming out from the sniper's gun
			this.ray = {
				x: this.x + (36 * Math.cos(angle)) + (13 * Math.sin(angle)),
				y: this.y + (36 * Math.sin(angle)) - (13 * Math.cos(angle)),
				distance: Phaser.Math.distance(this.x, this.y, game.input.x, game.input.y)
			}

			this.line.clear();

			// draw the ray only if the sniper isn't aiming too close
			if (this.ray.distance > 50){
				this.line.ctx.beginPath();
				this.line.ctx.moveTo(this.ray.x, this.ray.y);
				this.line.ctx.lineTo(game.input.x, game.input.y);
				this.line.ctx.lineWidth = 1;
				this.line.ctx.strokeStyle = 'darkred';
				this.line.ctx.stroke();
				this.line.ctx.closePath();
				this.line.render();
			}
		};
		p.shootRocket = function(){
			// don't shoot if the sniper is aiming too close
			if (this.ray.distance < 70){
				return;
			}

			// use a recycled rocket if there's one already used and stored in the rockets array
			var rocket = Sniper.rockets.pop() || new Game.classes.Rocket(this.x, this.y);
			rocket.rotation = this.rotateTo;
			
			// offset for the rocket so it would visually seem like coming out from the sniper's gun
			// (note: not equal to the ray's offset because rockets have anchor y point on their top)
			rocket.x = this.ray.x + (rocket.height * Math.cos(rocket.rotation - Math.PI * 0.5));
			rocket.y = this.ray.y + (rocket.height * Math.sin(rocket.rotation - Math.PI * 0.5));

			rocket.fireAt(game.input.x, game.input.y, this.ray.distance);
		};
		p.walkTo = function(x, y){
			// don't start walking if clicking on the sniper
			if (Phaser.Rectangle.contains(this, x, y)){
				return;
			}

			// distance between the current and the destination point
			var distance = Phaser.Math.distance(x, y, this.x, this.y);

			// number of frames needed to travel the distance
			var numFrames = distance / this.speed;

			// time interval between each frame (in milliseconds)
			var fi = 1000 / 60;

			// the duration needed for to travel the distance with the desired speed (in milliseconds)
			var duration = numFrames * fi;
			
			if (this.movementTween && this.movementTween.isRunning) {
				// stop the tween if it's already running to avoid running multiple conflicting tweens simultaneously
				this.movementTween.stop();
			}

			this.movementTween = this.game.add.tween(this).to({ x: x, y: y }, duration);
			this.movementTween.onStart.add(function(){
				this.animations.play('walking', 30, true);
			}.bind(this));
			
			this.movementTween.onComplete.add(function(){
				this.animations.play('idle');
			}.bind(this));
			
			this.movementTween.start();
		};
		p.update = function(){
			this.lookAtPointer();
			this.drawRay();
		};

		return Sniper;
	})(Game.self);
})(window.Game);