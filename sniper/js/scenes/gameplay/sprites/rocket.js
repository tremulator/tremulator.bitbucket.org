'use strict';
(function(Game){
	Game.classes.Rocket = (function(game){
		// constructor
		function Rocket(x, y){
			Phaser.Sprite.call(this, game, x, y, 'rocket');
			
			this.defineVars();

			this.anchor.setTo(0.5, 0);
			this.animations.add('moving');

			// each rocket has its own explosion
			this.explosion = new Game.classes.Explosion(x, y, this.hideForRecycling.bind(this));

			game.add.existing(this);
		}

		// extend Phaser.Sprite class
		var p = Rocket.prototype = Object.create(Phaser.Sprite.prototype);

		p.defineVars = function(){
			// distance in pixels to travel in each frame
			this.speed = 13;
			this.movingTween = undefined;
		};

		// public methods
		p.fireAt = function(x, y, distance){
			this.visible = true;
			this.animations.play('moving', 30, true);

			// number of frames needed to travel the distance
			var numFrames = distance / this.speed;

			// time interval between each frame (in milliseconds)
			var fi = 1000 / 60;

			// the duration needed to travel the distance with the desired speed (in milliseconds)
			var duration = numFrames * fi;
			
			this.movingTween = this.game.add.tween(this).to({ x: x, y: y }, duration);
			this.movingTween.onComplete.add(this.detonate, this);
			this.movingTween.start();
		};
		p.detonate = function(){
			this.visible = false;
			this.explosion.detonate(this.x, this.y);
		};
		p.hideForRecycling = function(){
			this.explosion.hide();
			var rocketsArray = Game.classes.Sniper.rockets;
			if (rocketsArray.indexOf(this) === -1){
				// store the rocket instance in the recycle array
				rocketsArray.push(this);
			}
		};

		return Rocket;
	})(Game.self)
})(window.Game);