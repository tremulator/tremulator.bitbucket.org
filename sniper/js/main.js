'use strict';
(function(){
	window.Game = window.Game || {
		self: new Phaser.Game(900, 600, Phaser.AUTO, 'myGame', {
			preload: function(){
				this.load.script('js/scenes/intro/intro');
				this.load.image('button', 'assets/button.png');
			},
			create: function(){
				this.state.start('intro');
			}
		}),
		classes: {
			// this is where classes will be stored
		}
	};
})();