'use strict';
(function(assets){
	// for some unknown reason, LabelTTF texts don't show when I use document.addEventListener('DOMContentLoaded', function(){
	window.onload = function(){
		cc.game.onStart = function(){
			window.addEventListener('blur', function(){
				myGame.pauseOnStart = true;
			});
			window.addEventListener('focus', function(){
				myGame.pauseOnStart = false;
			});

			cc.director.setDisplayStats(false);
			
			// create instance of loading scene
			var loadingScene = new myGame.LoadingSceneClass();

			// initially load only the assets necessary for the loading scene and run it as soon as they are loaded
			cc.loader.load(assets.getGroupArray('loading'), function(){
				// no need for code here
			}, function(){
				cc.director.runScene(loadingScene);
			});
		};
		cc.game.run('gameCanvas');
	};
})(myGame.assets);