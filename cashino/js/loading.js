'use strict';
(function(assets){
	myGame.LoadingSceneClass = cc.Scene.extend({
		onEnter: function(){
			this._super();
			this.size = cc.director.getWinSize();
			this.center = { x: this.size.width / 2, y: this.size.height / 2 };

			this._paintBackground();
			this._setLogo();
			this._setLoadingLabel();
			this._setProgressBar();
			this._scheduleLoading();
		},
		_paintBackground: function(){
			var bgLayer = cc.LayerColor.create(cc.color(101, 45, 144, 255));
			this.addChild(bgLayer);
		},
		_setLogo: function(){
			var logo = cc.Sprite.create(assets.getAsset('logo'));
			logo.setPosition(this.center.x, this.size.height * 0.6);

			// scaling would normally be dynamically calculated by screen size
			logo.setScale(0.33);

			this.addChild(logo);
		},
		_setLoadingLabel: function(){
			var loadingLabel = this.loadingLabel = cc.LabelTTF.create('Loading', 'Arial', 16);
			loadingLabel.setPosition(this.center.x, this.size.height * 0.26);
			loadingLabel.visible = false;
			this.addChild(loadingLabel);
		},
		_setProgressBar: function(){
			var scale = 0.33;
			var posY = this.size.height * 0.2;

			var maskNode = cc.Sprite.create(assets.getAsset('progress_bg'));
			maskNode.setPosition(this.center.x, posY);
			maskNode.setScale(scale);

			var progressBg = this.progressBg = cc.Sprite.create(assets.getAsset('progress_bg'));
			progressBg.setPosition(this.center.x, posY);
			progressBg.setScale(scale);

			var progressFill = this.progressFill = cc.Sprite.create(assets.getAsset('progress_fill'));
			progressFill.setPosition(this.center.x, posY);
			progressFill.setScale(scale);

			var mask = this.mask = cc.ClippingNode.create(maskNode);
			mask.setAlphaThreshold(0);
			mask.addChild(progressBg);
			mask.addChild(progressFill);

			mask.visible = false;

			this.addChild(mask);
		},
		_scheduleLoading: function(){
			// min: the x position of loadingLabel when the loading process is at 0%
			// max: the x position of loadingLabel when the loading process is at 100%
			var max = this.center.x;
			var min = max - (this.progressBg.width * this.progressBg.scaleX);

			// giving it a second for the title to be shown and then start loading the assets
			this.scheduleOnce(function(){
				this.loadingLabel.visible = true;
				this.mask.visible = true;
				this.progressFill.x = min;
				
				this._startLoading(min, max);
			}, 1);
		},
		_startLoading: function(min, max){
			var gameplayScene = new myGame.GameSceneClass();
			var transitionToGameplay = new cc.TransitionFade(1, gameplayScene);

			cc.loader.load(assets.getFullGameplayList(), function(data, count, loaded){
				if (cc.isObject(data) && data['machine-state']){
					myGame.results.setResults(data);
				}
				this.progressFill.x = Math.floor(min + (max - min) * (loaded / count));
			}.bind(this), function(){
				this.progressFill.x = max;
				this.mask.visible = false;
				this.loadingLabel.visible = false;

				cc.director.runScene(transitionToGameplay);
			}.bind(this));
		}
	});
})(myGame.assets);