'use strict';
(function(assets){
	var results;
	
	myGame.GameSceneClass = cc.Scene.extend({
		// set this to true to simulate a false result match (50-50 chance)
		test: true,
		onEnter: function(){
			this._super();

			this.paused = false;

			this.size = cc.director.getWinSize();
			this.center = { x: this.size.width / 2, y: this.size.height / 2 }; 

			results = myGame.results.getResults();

			this._setBackground();
			this._setReels();
			this._setButton();
			this._setCounter();
			this._setOverlay();

			if (myGame.pauseOnStart){
				this.pause();
			}

			window.addEventListener('blur', function(){
				this.pause();
			}.bind(this));
			window.addEventListener('focus', function(){
				this.resume();
			}.bind(this));
		},
		_setBackground: function(){
			var bg = this.bg = cc.Sprite.create(assets.getAsset('bg'));
			bg.setScale(this.size.width / bg.width);
			bg.setPosition(this.center.x, this.size.height / 2);
			bg.setOpacity(150);
			this.addChild(bg);
		},
		_setReels: function(){
			var position = { x: this.center.x, y: this.size.height * 0.57};
			var scale = 0.3;

			var maskNode = cc.Sprite.create(assets.getAsset('reels_mask'));
			maskNode.setColor(255, 255, 255, 255);
			maskNode.setPosition(position);
			maskNode.setScale(scale);

			var reels = cc.Sprite.create(assets.getAsset('reels'));
			reels.setScale(0.3);
			reels.setPosition(position);

			var mask = this.reelsContainer = new myGame.ReelContainerClass(maskNode);
			mask.setCenter(position);
			mask.setAlphaThreshold(0.1);
			mask.addChild(reels);
			
			this.addChild(mask);
		},
		_setButton: function(){
			var self = this;

			var btn = this.btn = new ccui.Button();
			btn.loadTextures(assets.getAsset('button_spin'));
			btn.setScale(0.35);
			btn.setPosition(this.size.width * 0.85, this.size.height * 0.57);
			
			btn.addClickEventListener(function(){
				self.disableButton();
				this.parent.spinReels();
			}, this);
			
			this.addChild(btn);
		},
		_setCounter: function(){
			var cntr = cc.Sprite.create(assets.getAsset('counter'));
			cntr.setScale(0.35);
			cntr.setPosition(this.center.x, this.size.height * 0.1);
			this.addChild(cntr);

			var winLabel = cc.LabelTTF.create('Win:', 'Arial', 14);
			winLabel.setPosition(this.center.x, this.size.height * 0.125);
			this.addChild(winLabel);

			var pointsCounter = this.pointsCounter = cc.LabelTTF.create('0', 'Arial', 16);
			pointsCounter.setPosition(this.center.x, this.size.height * 0.085);
			this.addChild(pointsCounter);
		},
		_setOverlay: function(){
			var overlay = this.overlay = cc.Sprite.create(assets.getAsset('bg'));
			overlay.setScale(this.size.width / overlay.width);
			overlay.setPosition(this.center.x, this.size.height / 2);
			overlay.setColor(255, 255, 255, 255);
			overlay.setOpacity(128);
			overlay.visible = false;
			this.addChild(overlay);
		},
		_combinations: [ undefined, undefined, undefined ],
		collectCombinations: function(i, comb){
			this._combinations[i] = comb.join(',');
			if (this._combinations.indexOf(undefined) === -1){
				this.reelsContainer.reportFullStop();
				this.checkResults(this._combinations.join(','));
				console.log(this._combinations.join(','));
			}
		},
		resetCombinations: function(){
			this._combinations = [ undefined, undefined, undefined ];
		},
		checkResults: function(result){
			var match = results.indexOf(result);
			if (match > -1 || this.test){
				if (match === -1 && this.test){
					// a false/simulated result match
					match = myGame.getRandom(0, results.length - 2);
				}

				var points = results[results.length - 1][match];

				if (points){
					// These will not be equal when this.test = true, it's just a simulation
					console.log('A ' + (match === -1 && this.test ? 'simulated ' : '') + 'match! ' + results[results.length - 1][match] + ' points won!');
					this.collectPoints(points);
					return;
				}else{
					console.log('A match with 0 points!');
				}
			}

			this.scheduler.schedule(function(){
				this.enableButton();
			}.bind(this), this, 0, 0, 1, !this.running);
		},
		disableButton: function(){
			this.btn.visible = false;
		},
		enableButton: function(){
			this.reelsContainer.hideReels();
			this.btn.visible = true;
		},
		spinReels: function(){
			this.reelsContainer.resetReelSequences();
			this.resetPoints();
			this.reelsContainer.showReels();

			this.scheduler.schedule(function(){
				this.reelsContainer.spinReels();
			}.bind(this), this, 0, 0, 0.5, !this._running);
		},
		collectPoints: function(points){
			var pointsCounter = this.pointsCounter;

			var incr = points / 100;
			var current = 0;

			this.scheduler.schedule(function(){
				myGame.sound.play('coins', true);

				current = Math.round(current + incr);
				pointsCounter.setString(current.toString());

				if (current >= points){
					pointsCounter.setString(points.toString());
					myGame.sound.stop('coins');
					this.enableButton();
					this.scheduler.unschedule('counting_points', this);
				}
			}.bind(this), this, 0.01, cc.REPEAT_FOREVER, 0.5, !this._running, 'counting_points');
		},
		resetPoints: function(){
			this.pointsCounter.setString('0');
		},
		pause: function(){
			if (this.paused) return;

			this._super();

			this.overlay.setVisible(true);

			this.paused = true;
			this.btn.pause();
			this.reelsContainer.pause();
		},
		resume: function(){
			if (!this.paused || !this.btn || !this.reelsContainer) return;
			
			this._super();
			this.paused = false;
			this.btn.resume();
			this.reelsContainer.resume();

			this.overlay.setVisible(false);
		}
	});
})(myGame.assets);