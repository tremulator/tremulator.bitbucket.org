'use strict';
myGame.sound = (function(assets){
	var _sounds = {};
	return {
		play: function(alias, repeat){
			_sounds[alias] = _sounds[alias] || {};

			if (repeat){
				if (_sounds[alias]['playing']){
					return;
				}
				_sounds[alias]['playing'] = true;
			}

			_sounds[alias]['sound'] = cc.audioEngine.playEffect(assets.getAsset(alias), repeat === true);
		},
		stop: function(alias){
			if (!_sounds[alias]) return;
			
			_sounds[alias]['playing'] = false;
			cc.audioEngine.stopEffect(_sounds[alias]['sound']);
		},
		resumeAll: function(){
			cc.audioEngine.resumeAllEffects();
		},
		pauseAll: function(){
			cc.audioEngine.pauseAllEffects();
		}
	}
})(myGame.assets);