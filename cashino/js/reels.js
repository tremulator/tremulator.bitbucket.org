'use strict';
(function(assets){
	var size, center;

	myGame.getRandom = function(min, max){
		var _min = Math.min(min, max);
		var _max = Math.max(min, max);
		return _min + Math.round(Math.random() * (_max - _min));
	};
	myGame.ReelContainerClass = cc.ClippingNode.extend({
		reels: [],
		reelCount: 3,
		onEnter: function(){
			this._super();
			size = cc.director.getWinSize();
			this._setReels();
		},
		_setReels: function(){
			// clipping nodes don't seem to have width, height or scale, so they will be used
			// from the reels image (this.children[0]) and used for scaling and aligning of
			// the reels and upon nesting their symbols
			var child = this.children[0];
			var actualSize = {
				width: child.width * child.getScale() / 3 - 5,
				height: child.height * child.getScale() / 3 - 3
			};

			var min = - actualSize.width;
			
			for (var i = 0; i < this.reelCount; i++){
				var reel = new myGame.ReelClass(i, actualSize);
				reel.x = i * actualSize.width + min;
				reel.y = - actualSize.height;
				this.reels.push(reel);
				this.addChild(reel);
			}
		},
		setCenter(cnt){
			center = cnt;
		},
		spinReels: function(){
			// delete last combinations recorded
			this.parent.resetCombinations();

			var count = 0;
			
			this.scheduler.schedule(function(dt){
				myGame.sound.play('spin', true);
				
				this.reels[count].startSpinning();
				count++;
				
				if (count === this.reels.length){
					// when the last reel starts spinning, a countdown starts for stopping the spinning
					this.scheduler.schedule(function(){
						this.stopReels();
					}.bind(this), this, 0, 0, 3, !this.running);
				}
			}.bind(this), this, 0.25, 2, 0, !this._running, 'start_spin');
		},
		stopReels: function(){
			var count = 0;
			this.scheduler.schedule(function(){
				this.reels[count].stopSpinning();
				count++;
			}.bind(this), this, 0.25, 2, 0, !this.running, 'stop_spin');
		},
		isBusy: function(){
			for (var i = 0; i < this.reels.length; i++){
				if (this.reels[i].spinning){
					return true;
				}
			}
			return false;
		},
		hideReels: function(){
			for (var i = 0; i < this.reels.length; i++){
				var reel = this.reels[i];
				reel.visible = false;
			}
		},
		showReels: function(){
			for (var i = 0; i < this.reels.length; i++){
				var reel = this.reels[i];
				reel.visible = true;
			}
		},
		resetReelSequences: function(){
			for (var i = 0; i < this.reels.length; i++){
				var reel = this.reels[i];
				if (!reel.sequenceSet || !reel.sequenceDrawn){
					// reset the sequence on every new spin
					reel.resetSequence();
				}
			}
		},
		reportFullStop: function(){
			myGame.sound.stop('spin');
		},
		pause: function(){
			this._super();
			for (var i = 0; i < this.reels.length; i++){
				var reel = this.reels[i];
				reel.pause();
				myGame.sound.pauseAll();
			}
		},
		resume: function(){
			this._super();
			for (var i = 0; i < this.reels.length; i++){
				var reel = this.reels[i];
				reel.resume();
				myGame.sound.resumeAll();
			}
		}
	});
	myGame.ReelClass = cc.Layer.extend({
		ctor: function(i, actualSize){
			this._super();
			this.number = i;
			this.actualSize = actualSize;
		},
		onEnter: function(){
			this._super();
		},
		_setSequence: function(){
			var random = myGame.getRandom;

			var keys = Object.keys(myGame.assets.getGroupObj('symbols'));
			var range = keys.length - 1;

			// generate a new random sequence of symbols
			this.sequence = [];
			for (var i = 0; i < 23; i++){
				// This means that the first 20 will be created and the last 3 will be copied from the first 3 for smooth loop effect
				this.sequence.push(this.sequence[i % 20] || keys[random(0, range)].toUpperCase());
			}

			this.sequenceSet = true;
		},
		// draw the generated sequence
		_drawSequence: function(){
			var seq = this.sequence;
			for (var i = 0; i < seq.length; i++){
				var alias = seq[i].toLowerCase();

				var symbol = this.symbols ? this.symbols[i] : undefined;

				if (symbol){
					symbol.setTexture(assets.getAsset(alias));
					symbol.custom.alias = alias;
					symbol.custom.index = i;
					continue;
				}
				
				symbol = cc.Sprite.create(assets.getAsset(alias));
				symbol.custom = {
					alias: alias.toUpperCase(),
					index: i
				};
				this.symbols = this.symbols || [];
				this.symbols.push(symbol);

				symbol.setScale(0.3);

				// I use unnecessary brackets in math operations just for visual clarity and readability
				symbol.setPosition(center.x, center.y + (this.actualSize.height * seq.length) - (this.actualSize.height * (i + 1)));
				this.addChild(symbol);				
			}
			this.sequenceDrawn = true;
		},
		_spinning: (function(){
			var max, min;
			return {
				start: function(){
					var self = this;

					this.decr = 15;

					self.y = -self.actualSize.height;
					max = self.y;
					min = self.y - self.actualSize.height * 20;
					self.spinning = true;

					//(callback, target, interval, repeat, delay, paused, key){
					self.scheduler.schedule(function(){
						var newDecr = Math.min(this.decr, Math.abs(min - self.y));
						if (newDecr < this.decr){
							// return the reel to its starting position
							self.y = max;
						}else {
							self.y -= newDecr;
						}
					}, self, 1/80, cc.REPEAT_FOREVER, 0, !self._running, 'spinning');
				},
				stop: function(){
					var self = this;
					self.scheduler.schedule(function(){
						this.decr -= Math.min(this.decr, 1);
						if (this.decr === 0){
							// the nearest next grid to align the reel to
							var toNextGrid = (max - Math.ceil((max - self.y) / self.actualSize.height) * self.actualSize.height) - self.y;

							if (toNextGrid < 0){
								self.y -= Math.min(4, Math.abs(toNextGrid));
							}else if (toNextGrid === 0){
								self.combinationIndex = self.sequence.length - Math.round((max - self.y) / self.actualSize.height) - 3;
								myGame.sound.play('spin_end');
								
								// This is the final combination array for this reel. This will be compared against the data in results.json
								self.combination = self.sequence.splice(self.combinationIndex, 3);
								
								this.spinning = false;
								this.sequenceSet = false;
								this.sequenceDrawn = false;

								self.scheduler.unschedule('decelerating', self);
								self.scheduler.unschedule('spinning', self);

								var mainGameplay = self.parent.parent;
								mainGameplay.collectCombinations(self.number, self.combination);
							}
						}
					}, self, 1/60, cc.REPEAT_FOREVER, 0, !self._running, 'decelerating');
				}
			}
		})(),
		startSpinning: function(){
			this._spinning.start.call(this);
		},
		stopSpinning: function(){
			this._spinning.stop.call(this);
		},
		resetSequence: function(){
			this._setSequence();
			this._drawSequence();
		},
	});
})(myGame.assets);