'use strict';
// Organizing the asset list for convenient loading and referencing during gameplay
window.myGame = window.myGame || {
	assets: (function(){
		var graphArrs = {
			loading: [
				'graphics/loading/logo.png',
				'graphics/loading/progress_bg.png',
				'graphics/loading/progress_fill.png'
			],
			gameplay: [
				'graphics/gameplay/bg.jpg',
				'graphics/gameplay/button_spin.png',
				'graphics/gameplay/counter.png',
				'graphics/gameplay/reels.png',
				'graphics/gameplay/reels_mask.png',
			],
			results: [
				'results.json'
			],
			symbols: [
				'graphics/gameplay/symbols/WILD.png',
				'graphics/gameplay/symbols/H1.png',
				'graphics/gameplay/symbols/H2.png',
				'graphics/gameplay/symbols/H3.png',
				'graphics/gameplay/symbols/H4.png',
				'graphics/gameplay/symbols/L1.png',
				'graphics/gameplay/symbols/L2.png',
				'graphics/gameplay/symbols/L3.png',
				'graphics/gameplay/symbols/L4.png',
			],
			sfx: [
				'sfx/coins.wav',
				'sfx/spin.wav',
				'sfx/spin_end.wav'
			]
		};

		var graphObj = (function(arrs){
			var output = {};
			for (var k in arrs){
				var arr = arrs[k];
				for (var i = 0; i < arr.length; i++){
					var el = arr[i];

					// generating a short alias for each file
					var key = el.split('/').reverse()[0].split('.')[0].toLowerCase();

					output[k] = output[k] || {};
					output[k][key] = el;
				}
			}
			return output;
		})(graphArrs);

		return {
			// Get an array of asset URLs by scene alias/key. Useful for loading the graphics for a specific scene.
			getGroupArray: function(group){
				return graphArrs[group];
			},
			getGroupObj: function(group){
				return graphObj[group];
			},
			// Get the desired asset by its short alias/key, rather than its full path. Convenient for creating sprites upon constructing a scene or during gameplay.
			getAsset: function(key){
				return graphObj['loading'][key] || graphObj['gameplay'][key] || graphObj['symbols'][key] || graphObj['sfx'][key];
			},
			getFullGameplayList: function(){
				var keys = Object.keys(graphArrs);
				var arr = [];
				for (var i = 0; i < keys.length; i++){
					var key = keys[i]
					if (key !== 'loading'){
						arr = arr.concat(graphArrs[key]);
					}
				}
				return arr;
			}
		};
	})(),
	results: (function(){
		var _results = [];
		return {
			setResults: function(data){
				// Parsing the data for convenience when comparing results
				data = data['machine-state'];
				var wins = [];
				data = data.map(function(el){
					wins.push(el.win);
					var comb = [];
					var reels = el.reels;
					for (var i = 0; i < reels.length; i++){
						comb.push(reels[i].join(','));
					}
					return comb.join(',');
				});
				data.push(wins);
				_results = data;
			},
			getResults: function(){
				return _results;
			}
		}
	})()
};