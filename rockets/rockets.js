// created by Vladimir Dimitrovski, 01.2016
'use strict';
(function(){
	var fps = 60;

	var getRandom = function(min, max){
		var _min = Math.min(min, max);
		var _max = Math.max(min, max);
		return _min + Math.round(Math.random() * (_max - _min));
	};
	var for2d = function(countX, countY, fn){
		if (!fn && countY instanceof Function){
			fn = countY;
			countY = 1;
		}
		for (var i = 0; i < countX; i++){
			for (var j = 0; j < countY; j++){
				if (fn){
					fn(i, j);
				}
			}
		}
	}

	document.addEventListener("DOMContentLoaded", function(e){
		var canvas = document.getElementById('theCanvas');
		var scene = canvas.getContext('2d');

		var Square = (function(){
			var _nodeValues = [
				// l = left, r = right, a = above, b = below
				// index values 0: 0 degrees, 1: 90 degrees, 2: 180 degrees, 3: 270 degrees 
				['lr', 'ab', 'lr', 'ab'],
				['lb', 'la', 'ra', 'rb'],
				['lrb', 'lab', 'lra', 'rab'],
				['lrab', 'lrab', 'lrab', 'lrab']
			];
			
			var getColor = function(c) {
				var colors = {
					n: 'black', // not connected
					l: 'green', // connected from the left side
					r: 'olive', // connected from the right side
					b: 'darkblue', // connected from both sides
					f: 'red' // fired up
				};

				if (!colors[c]){
					console.error('Color with ID "' + c + '"" doesn\'t exist');
					return undefined;
				}
				return colors[c];
			}
			
			var fireSpeed = 50;

			function Square(h, v, size){
				this.size = size || 50;
				this.h = h;
				this.v = v;
				this.x = h * this.size;
				this.y = v * this.size;
				this.centerX = this.x + this.size / 2;
				this.centerY = this.y + this.size / 2;
				this.timeout = undefined;
				
				this.chooseShape();
			}

			var p = Square.prototype;
			p.chooseShape = function(){
				this.shape = [getRandom(0, 2), getRandom(0, 2), getRandom(0, 3)][getRandom(0, 2)];
				this.rotIdx = getRandom(0, 3);
				this.rotation = this.rotIdx * 90;
				this.nodes = _nodeValues[this.shape][this.rotIdx];
			};
			p.resetStatus = function(){
				playfield.unstore(this);

				this.nodeColor = getColor('n');
				this.lighter = false;
				this.connections = [];
				this.scanned = false;
				this.status = 'n';

				this.draw();
			};
			p.setNodeColor = function(status){
				this.nodeColor = getColor(status);
			};
			p.setY = function(y){
				this.clear();
				this.y = y;
				this.centerY = this.y + this.size / 2;

				this.draw();
			};
			p.setRotation = function(rot){
				this.clear();
				this.rotation = rot;
				this.draw();
			};
			p.canvas = function(action){
				scene.save();
				scene.translate(this.centerX, this.centerY);
				var size = this.size;
				var x = -size / 2;
				var y = -size / 2;
				return {
					clear: function clear(){
						scene.clearRect(x, y, size, size);
						scene.restore();
					}.bind(this),
					draw: function draw(){
						scene.beginPath();
						scene.rect(x, y, size, size);
						scene.closePath();
						scene.clip();

						scene.rotate(this.rotation * Math.PI / 180);
						scene.fillStyle = 'black';
						scene.fillRect(x, y, size, size);
						scene.fillStyle = 'yellow';
						scene.fillRect(x + 2, y + 2, size - 4, size - 4);
						scene.fillStyle = this.nodeColor;
						if (this.shape === 0){
							scene.fillRect(x, -(size / 6), size, size / 3);
						}else if (this.shape === 1){
							scene.fillRect(x, -(size / 6), size / 2, size / 3);
							scene.fillRect(-(size / 6), -(size / 6), size / 3, size / 2 + size / 6);
						}else if (this.shape === 2){
							scene.fillRect(x, -(size / 6), size, size / 3);
							scene.fillRect(-(size / 6), -(size / 6), size / 3, size / 2 + size / 6);
						}else if (this.shape === 3){
							scene.fillRect(x, -(size / 6), size, size / 3);
							scene.fillRect(-(size / 6), -(size / 2), size / 3, size);
						}
						scene.restore();
					}.bind(this)
				};
			};
			p.clear = function(){
				this.canvas().clear();
			};
			p.draw = function(){
				this.canvas().draw();
			};
			p.rotate = (function(){
				var interval;
				var incr = 9;
				return function(){
					playfield.abortFire();
					playfield.busy = true;
					var endAt = this.rotation + 90;
					clearInterval(interval);
					interval = setInterval(function(){
						this.setRotation(this.rotation + Math.min(incr, endAt - this.rotation));
						if (endAt - this.rotation === 0){
							clearInterval(interval);
							this.rotIdx = (this.rotIdx + 1) % 4;
							this.rotation = this.rotIdx * 90;
							this.nodes = _nodeValues[this.shape][this.rotIdx];
							playfield.scan();
						}
					}.bind(this), 1000 / fps);
				};
			})();

			p.hasNode = function(node){
				return this.nodes.indexOf(node) >= 0;
			};
			p.setStatus = function(newStatus, caller){
				if (this.status === 'b'){
					if (newStatus === 'l' || newStatus === 'r'){
						if (this.lighter){
							playfield.fireUp(this);
						}
						return;
					}
				}

				if (caller) this.addToConnections(caller);
				
				this.scanned = true;
				this.status = newStatus;
				this.nodeColor = getColor(newStatus);

				if (newStatus === 'l' || newStatus === 'r'){
					playfield.store(this);
				}
				else if (newStatus === 'b'){
					playfield.connect(this);
					if (this.lighter){
						playfield.fireUp(this);
					}
				}

				this.draw();

				this.lookForConnections();
			};
			p.lookForConnections = function(){
				var neighbours = this.getNeighbours();

				var otherStatus = 'lr'.replace(this.status, '');
				for (var idx in neighbours){
					var neighbour = neighbours[idx];
					// access only existing neighbours (i.e. square with h=0 and v=0 doesn't have a neighbour
					// from left or above, so undefined is an expected value for the neighbour)
					if (neighbour){
						var myNode = idx.charAt(0);
						var theirNode = idx.charAt(1);
						if (this.hasNode(myNode) && neighbour.hasNode(theirNode)){
							this.addToConnections(neighbour);
							if (!neighbour.scanned){
								neighbour.setStatus(this.status, this);
							}else if (neighbour.status === otherStatus){
								neighbour.setStatus('b', this);
							}else if (this.status === 'b' && neighbour.status !== 'b'){
								neighbour.setStatus('b', this);
							}
						}
					}
				}
			};
			p.addToConnections = function(square){
				if (this.connections.indexOf(square) === -1){
					this.connections.push(square);
				}
			};
			p.getNeighbours = function(){
				return {
					lr: playfield.getSquare(this.h - 1, this.v), // neighbour from left
					rl: playfield.getSquare(this.h + 1, this.v), // neighbour from right
					ab: playfield.getSquare(this.h, this.v - 1), // neighbour from above
					ba: playfield.getSquare(this.h, this.v + 1) // neighbour from below
				};
			};
			p.fireUp = function(){
				if (this.status === 'f'){
					return;
				}

				this.status = 'f';
				this.nodeColor = getColor(this.status);
				this.draw();

				// it means that this is the last square to be fired up
				if (playfield.connected.length === 0){
					playfield.abortFire();
					playfield.reorder();
				}else{
					clearTimeout(this.timeout);
					this.timeout = setTimeout(function(){
						for2d(this.connections.length, function(i){
							var con = this.connections[i];
							playfield.burn(con);
							con.fireUp();
						}.bind(this));
					}.bind(this), fireSpeed);
				}
			};
			p.abortFire = function(){
				clearTimeout(this.timeout);
			};
			p.startScan = function(node){
				if (this.hasNode(node)){
					if (node === 'l'){
						this.lighter = true;
					}
					this.setStatus(node);
				}
			};

			return Square;
		})();

		var playfield = (function(){
			var hCount = 6;
			var vCount = 9;
			var sqSize = 50;

			var timeout;
			var interval;
			var fireUpTime = 1000;

			return {
				busy: false,
				squares: (function(){
					var _squares = [];
					for2d(hCount, vCount, function(h, v){
						_squares[h] = _squares[h] || [];
						_squares[h].push(new Square(h, v, sqSize));
					});
					return _squares;
				})(),
				organizeStorage: function(square, arr1, arr2){
					var isAdded = false;

					if (arr1.indexOf(square) === -1){
						arr1.push(square);
						
						isAdded = true;

						if (arr2){
							var idx = arr2.indexOf(square);
							if (idx >= 0){
								arr2.splice(idx, 1);
							}
						}
					}

					return isAdded;
				},
				unstored: [],
				unstore: function(square){
					if (this.organizeStorage(square, this.unstored, this.stored)){
						if (this.busy){
							if (this.unstored.length === hCount * vCount){
								this.busy = false;
							}
						}
						return true;
					}
					return false;
				},
				stored: [],
				store: function(square){
					return this.organizeStorage(square, this.stored, this.unstored);
				},
				connected: [],
				connect: function(square){
					return this.organizeStorage(square, this.connected, this.stored);
				},
				burned: [],
				burn: function(square){
					if (!this.burned[square.h]){
						this.burned[square.h] = [];
					}
					return this.organizeStorage(square, this.burned[square.h], this.connected);
				},
				reset: function(){
					this.unstored = [];
					this.stored = [];
					this.connected = [];
					this.burned = [];
					for2d(hCount, vCount, function(h, v){
						this.getSquare(h, v).resetStatus();
					}.bind(this));
				},
				fireUp: function(square){
					// if adding is successful
					if (this.burn(square)){
						clearTimeout(timeout);
						timeout = setTimeout((function(){
							this.busy = true;
							for (var i = 0; i < this.burned.length; i++){
								for (var j = 0; j < this.burned[i].length; j++){
									var target = this.burned[i][j];
									target.fireUp();
								}
							}
						}).bind(this), fireUpTime);
					}
				},
				scan: function(){
					this.busy = true;
					this.reset();
					
					for2d(vCount, function(v){
						var leftSq = this.getSquare(0, v);
						var rightSq = this.getSquare(hCount - 1, v);
						leftSq.startScan('l');
						rightSq.startScan('r');
					}.bind(this));
				},
				getSquare: function(h, v){
					if (!this.squares[h] || !this.squares[h][v]) return undefined;
					return this.squares[h][v];
				},
				abortFire: function(){
					clearTimeout(timeout);
					clearInterval(interval);
					for2d(hCount, vCount, function(h, v){
						this.squares[h][v].abortFire();
					}.bind(this));
				},
				reorder: function(){
					var movers = [];
					for2d(hCount, vCount, function(h, v){
						var length = this.burned[h].length;
						var square = this.squares[h][v];
						if (square.status === 'f'){
							var newV = this.burned[h].indexOf(square);
							square.setY((newV - length) * sqSize);
							square.chooseShape();
							square.setNodeColor('n');
							movers.push({ square: square, newV: newV, moved: false });
						}else{
							var newV = this.burned[h].length;
							this.burned[h].push(square);
							if (square.v !== newV){
								movers.push({ square: square, newV: newV, moved: false });
							}
						}
					}.bind(this));
					this.moveSquares(movers);
				},
				moveSquares: function(movers){
					var moverCount = movers.length;
					var incr = 1;
					clearInterval(interval);
					interval = setInterval(function(){
						if (moverCount > 0){
							for2d(movers.length, function(i){
								if (movers[i].moved) return;

								var mover = movers[i];
								var square = mover.square;
								var newV = mover.newV;
								var endAt = newV * sqSize;

								square.setY(square.y + Math.min(incr, endAt - square.y));
								
								if (endAt - square.y <= 0){
									square.v = newV;
									this.squares[square.h][square.v] = square;
									mover.moved = true;
									moverCount--;
								}
							}.bind(this));
							incr += 2;
						}else{
							clearInterval(interval);
							this.scan();
						}
					}.bind(this), 1000 / fps);
				},
				click: function(mouseX, mouseY){
					if (this.busy) return;
					var h = Math.floor(Math.min(canvas.width - 1, mouseX) / sqSize);
					var v = Math.floor(Math.min(canvas.height - 1, mouseY) / sqSize);
					var target = this.getSquare(h, v);
					
					if (target) target.rotate();
					return target;
				}
			}
		})();

		playfield.scan();

		canvas.addEventListener('click', function(e){
			playfield.click(e.offsetX, e.offsetY);
		});
	});
})();